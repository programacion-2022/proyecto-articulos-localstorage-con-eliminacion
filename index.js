//DECLARO EL ARRAY DE PRODUCTOS, VACÍO.
let arrayProductos = [];
//DECLARO EL FORMATO DEL PRODUCTO, NO NECESARIO, PERO INFORMATIVO
let producto = {
    descripcion: "",
    imagen: ""
}
if (localStorage.getItem('productos')) {
    arrayProductos = JSON.parse(localStorage.productos);
    console.log(arrayProductos);
} else {
    //ASIGNO MANUALMENTE EL PRIMER PRODUCTO
    //fuente de productos: https://revistabyte.es/recomendamos/productos-tic-mas-vendidos-amazon/

    producto = {
        descripcion: "Fire TV Stick 4K Ultra HD con mando por voz Alexa",
        imagen: "https://revistabyte.es/wp-content/uploads/2020/05/1-323x360.jpg.webp"
    }
    //LO AGREGO AL ARRAY
    arrayProductos.push(producto);

    //ASIGNO MANUALMENTE EL SEGUNDO PRODUCTO
    producto = {
        descripcion: 'TV Samsung 4K UHD de 50”',
        imagen: "https://revistabyte.es/wp-content/uploads/2020/05/4-425x360.jpg.webp"
    }
    arrayProductos.push(producto);
}
//INYECTO LOS PRODUCTOS DEL ARRAY EN LA SECCIÓN PRODUCTO
document.querySelector("#producto").innerHTML = armaTemplate();


//FUNCIÓN QUE SE INVOCA DESDE LA INTERFAZ PARA CARGAR NUEVOS ARTÍCULOS
function agregar() {
    //TOMO LOS VALUES DE DESCRIPCIÓN E IMAGEN DE LA INTERFAZ
    let des = document.querySelector("#desc").value.trim();
    let img = document.querySelector("#dirImg").value.trim();
    //PARA EVALUAR QUE HAYA INGRESADO ALGO
    if (des.length === 0 || img.length === 0) return;

    //ARMO EL OBJETO
    producto = {
        descripcion: des,
        imagen: img
    }
    //AGREGO EL PRODUCTO AL ARRAY
    arrayProductos.push(producto);
    console.log(arrayProductos);

    //SIGO EL MISMO PROCEDIMIENTO QUE CUANDO SE HACÍA MANUAL, LÍNEA 112
    document.querySelector("#producto").innerHTML = armaTemplate();
}

//FUNCION QUE ARMA EL TEMPLATE DE ARTÍCULOS CON LOS PRODUCTOS DEL ARRAY Y RETORNA EL TEMPLATE
function armaTemplate() {
    let template = '';
    for (let i = 0; i < arrayProductos.length; i++) {
        producto = arrayProductos[i];
        template += `<article>
                        <div class="trash" onclick="eliminarItem(${i})"><img src="trash-can.png"></div>
                        <h3 class="descripcion">${producto.descripcion}</h3>
                        <img src="${producto.imagen}" class="imagen">
                    </article>`
    }
    return template;
}

function listado() {
    if (arrayProductos.length > 0) localStorage.setItem("productos", JSON.stringify(arrayProductos));
    location.href = "resultados.html";
}
function eliminarItem(nroProd){
    console.log("Hice click en el producto: ",nroProd);
    arrayProductos.splice(nroProd,1);
    if(arrayProductos.length === 0) localStorage.removeItem('productos');
    document.querySelector("#producto").innerHTML = armaTemplate();
}